﻿using UnityEngine;
using System.Collections;

public class DestroyOnExit : MonoBehaviour {

	void OnTriggerExit(Collider collider){
		if(collider.tag != "Player"){
			Destroy(collider.gameObject);
		}		
	}
}
