﻿using UnityEngine;
using System.Collections;

public class PlayerShoot : MonoBehaviour {

	private VirtualController virtualController;
	private Vector3 targetDirection;
	private Weapon weapon;
	
	// Use this for initialization
	void Start () {
		virtualController = FindObjectOfType<VirtualController> ();
		weapon = GetComponent<Weapon>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		
		targetDirection = Vector3.Normalize(virtualController.getRightStick());
		if(targetDirection != Vector3.zero){
			weapon.shoot(transform.position,targetDirection);
			
		}

		
	}
}
