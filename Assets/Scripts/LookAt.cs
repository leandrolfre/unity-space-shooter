﻿using UnityEngine;
using System.Collections;

public class LookAt : MonoBehaviour {

	public Transform target;
	private Vector3 newPos;
	public float speed;
	
	void Awake(){
		newPos = new Vector3 (target.position.x,transform.position.y,target.position.z);
	}
	
	void FixedUpdate ()
	{	
		newPos.x = target.position.x;
		newPos.y = transform.position.y;
		newPos.z = target.position.z;
	
		transform.position = Vector3.Lerp (transform.position,newPos,Time.deltaTime*speed);
	}                    // The initial offset from the target.
}
