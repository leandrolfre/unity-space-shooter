﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour {

	public float turnSmoothing = 15f;
	public float maxSpeed = 15.0f;
	private Vector3 targetDirection;

	private VirtualController virtualController;

	// Use this for initialization
	void Start () {
		virtualController = FindObjectOfType<VirtualController> ();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		targetDirection = virtualController.getLeftStick ();
		rotate ();
		move ();
	}
	
	private void move(){		
		GetComponent<Rigidbody>().velocity = targetDirection * Time.deltaTime * maxSpeed;	
	}
	
	private void rotate(){
		Quaternion targetRotation = Quaternion.LookRotation (targetDirection, Vector3.up);
		Quaternion newRotation = Quaternion.Lerp(GetComponent<Rigidbody>().rotation, targetRotation, turnSmoothing * Time.deltaTime);		
		GetComponent<Rigidbody>().MoveRotation (newRotation);
	}
}
