﻿using UnityEngine;
using System.Collections;

public class ThumbMotion : MonoBehaviour {


	private RectTransform leftStickRect;
	private RectTransform rightStickRect;
	public float maxDistance = 60.0f;

	// Use this for initialization
	void Start(){
		
		Transform canvas = transform.FindChild ("Canvas");
		Transform leftStick = canvas.FindChild ("leftStickBackground").FindChild("leftStick");
		Transform rightStick = canvas.FindChild ("rightStickBackground").FindChild("rightStick");



		leftStickRect = leftStick.GetComponent<RectTransform> ();
		rightStickRect = rightStick.GetComponent<RectTransform> ();	
		
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 leftStick = GetComponent<VirtualController> ().getLeftStick ();
		Vector3 rightStick = GetComponent<VirtualController> ().getRightStick ();

		Vector3 leftLocalPos = new Vector3 (leftStick.x, leftStick.z, 0.0f);
		Vector3 rightLocalPos = new Vector3 (rightStick.x, rightStick.z, 0.0f);

		leftStickRect.localPosition = leftLocalPos*maxDistance;
		rightStickRect.localPosition = rightLocalPos*maxDistance;

	}
}
