﻿using UnityEngine;
using System.Collections;

public class VirtualController : MonoBehaviour {

	private static float centerScreen = Screen.width*0.5f;
	private Vector3 leftStickPos;
	private Vector3 rightStickPos;
	private const float MAX_SIZE = 100.0f;

	void Start(){

		Transform canvas = transform.FindChild ("Canvas");

		RectTransform rc = canvas.FindChild("leftStickBackground").GetComponent<RectTransform>();
		leftStickPos = rc.position;

		rc = canvas.FindChild("rightStickBackground").GetComponent<RectTransform>();
		rightStickPos = rc.position;

	}

	public Vector3 getLeftStick(){

		foreach (Touch touch in Input.touches) {
			if (isMotionPhase(touch) && touch.position.x < centerScreen){
				return getTouchPosition(touch,leftStickPos);
			}			
		}
		return Vector3.zero;
	}

	public Vector3 getRightStick(){
		
		foreach (Touch touch in Input.touches) {
			if (isMotionPhase(touch) && touch.position.x > centerScreen){
				return getTouchPosition(touch,rightStickPos);
			}			
		}		
		return Vector3.zero;
	}

	private bool isMotionPhase(Touch touch){
		return (touch.phase == TouchPhase.Moved || touch.phase == TouchPhase.Stationary);
	}

	private Vector3 getTouchPosition(Touch touch, Vector3 offset){
		return Vector3.ClampMagnitude(new Vector3(touch.position.x - offset.x, 0.0f, touch.position.y - offset.y),MAX_SIZE)/MAX_SIZE;
	}

}
