﻿using UnityEngine;
using System.Collections;

public enum Damage{
	LOW,
	MEDIUM,
	MEDLOW,
	HIGH
};

public enum FireRate{
	SLOW,
	MEDIUM,
	FAST	
};

public class Weapon : MonoBehaviour {

	public Rigidbody shot;
	public FireRate fireRate;
	public Damage damage;
	private float lastShoot = 0.0f;
	public float speed = 15.0f;
	
	
	public void shoot(Vector3 position, Vector3 direction){
		
			Quaternion targetRotation = Quaternion.LookRotation (direction, Vector3.up);			
			Rigidbody projectile = Instantiate(shot,position,targetRotation) as Rigidbody;						
			projectile.velocity = direction * speed;			
		
		
	}
	
}
