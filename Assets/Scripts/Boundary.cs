﻿using UnityEngine;
using System.Collections;

public class Boundary : MonoBehaviour {

	
	public Transform[] clampedObjects;
	private BoxCollider border;
	private Vector3 tempPos;
	private Vector3 offset;
	
	void Awake(){
		border = GetComponent<BoxCollider>();
		offset = new Vector3(2,0,2);
	}
	
	void Start(){
		GameObject background = GameObject.Find("Background");
		float aspectRatio = Camera.main.aspect;
		float orthoSize = Camera.main.orthographicSize;
		float middleWidth = (background.transform.localScale+offset).x * 0.5f;
		
//		print ("AspectRatio:"+aspectRatio);
//		print ("OrthoSize:"+orthoSize);
//		print ("middleWidth:"+middleWidth);
//		print ("formula:"+(middleWidth - aspectRatio * orthoSize)/middleWidth);		
		if(background != null){
			if(transform.parent != null){
				transform.localScale = new Vector3(
					(middleWidth - aspectRatio * orthoSize)/middleWidth,
					transform.localScale.y,
					transform.localScale.z*0.55f
				); 
			}else{
				Vector3 tempScale = background.transform.localScale;
				transform.localScale = new Vector3(tempScale.x,tempScale.z,tempScale.y)-offset;
			}
		}
	}
	
	void FixedUpdate(){	
		
//		print (name +"- bounds size:"+border.bounds.size);
//		print (name +"- min:"+border.bounds.min);
//		print (name +"- max:"+border.bounds.max);
//		print ("Background:"+GameObject.Find("Background").transform.localScale);
		foreach(Transform t in clampedObjects){
			tempPos.x = Mathf.Clamp(t.position.x,border.bounds.min.x,border.bounds.max.x);
			tempPos.y = t.position.y;
			tempPos.z = Mathf.Clamp(t.position.z,border.bounds.min.z,border.bounds.max.z);
			
			t.position = tempPos;
		}
	}
}
